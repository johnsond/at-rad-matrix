#!/bin/bash

# IPs to allow through proxy
ALLOWED_IPADDRS=()

# Addresses to NAT (public,private)
NAT_ADDRS=()

#
# Grab interface info
#
CIF=`cat /var/emulab/boot/controlif`
#
# Quite a hack, but workable.  Assume FH links and proxy/gw addrs are
# 192.168.x.1 .
#
GWADDRS=`ip -br a | grep 192.168. | sed -nre 's/^.*(192\.168\.[0-9]*\.1)\/.*$/\1/p' | xargs`
GWIFS=""
for gwa in $GWADDRS ; do
    gwif=`/usr/local/etc/emulab/findif -i $gwa`
    GWIFS="$GWIFS $gwif"
done

if [ -z "$GWADDRS" -o -z "$GWIFS" ]
then
    echo "Could not find interfaces for running dhcpd!"
    exit 1
fi

#
# Enable NAT and firewall setup
#

# Forwarding firewall rules.  Allow established connections, and new
# connections on specific ports (e.g. SSH port 22). Allow out all
# traffic from the internal network.
sudo iptables -F FORWARD
sudo iptables -P FORWARD DROP
for ipa in ${ALLOWED_IPADDRS[@]}; do
    for gwif in $GWIFS ; do
	sudo iptables -A FORWARD -i $CIF -o $gwif -s $ipa -j ACCEPT
	sudo iptables -A FORWARD -i $gwif -o $CIF -d $ipa -j ACCEPT
    done
done
for gwif in $GWIFS ; do
    sudo iptables -A FORWARD -i $CIF -o $gwif -s 155.98.32.70 -p udp --sport 53 -j ACCEPT
    sudo iptables -A FORWARD -i $gwif -o $CIF -d 155.98.32.70 -p udp --dport 53 -j ACCEPT
    sudo iptables -A FORWARD -i $CIF -o $gwif -s 155.98.33.74 -p udp --sport 123 -j ACCEPT
    sudo iptables -A FORWARD -i $gwif -o $CIF -d 155.98.33.74 -p udp --dport 123 -j ACCEPT
done

# Proxy ARP entries for the two 1-to-1 NAT addresses
# ! This never worked right, so IP aliases are used instead.
#sudo arp -i $CIF -Ds 155.98.36.197 $CIF pub
#sudo arp -i $CIF -Ds 155.98.36.198 $CIF pub

# Iterate over and set up 1-to-1 NAT pairs
sudo iptables -t nat -F
for apair in ${NAT_ADDRS[@]}; do
    tmparr=(${apair//,/ })
    pubaddr=${tmparr[0]}
    privaddr=${tmparr[1]}

    # Add IP alias for the public address
    sudo ip addr add $pubaddr dev $CIF

    # Set up 1-to-1 NAT for the device.
    sudo iptables -t nat -A POSTROUTING -o $CIF -s $privaddr -j SNAT --to-source $pubaddr
    sudo iptables -t nat -A PREROUTING -i $CIF -d $pubaddr -j DNAT --to-destination $privaddr
done

# Tell kernel to forward packets
sudo sysctl -w net.ipv4.ip_forward=1

#
# Set up the DHCP server
#
sudo apt-get -q update && \
    sudo apt-get -q -y install --reinstall isc-dhcp-server || \
    { echo "Failed to install ISC DHCP server!" && exit 1; }

sudo cp -f /local/repository/etc/dhcpd.conf /etc/dhcp/dhcpd.conf || \
    { echo "Could not copy dhcp config file into place!" && exit 1; }

for gwa in $GWADDRS ; do
    gwnet=`echo $gwa | sed -nre 's/^192\.168\.([0-9]*)\.1$/\1/p'`
    cat <<EOF | sudo tee -a /etc/dhcp/dhcpd.conf
subnet 192.168.$gwnet.0 netmask 255.255.255.0 {
    range 192.168.$gwnet.101 192.168.$gwnet.200;
    option routers 192.168.$gwnet.1;
    option domain-name-servers 155.98.32.70;
    option ntp-servers 155.98.33.74;
}

EOF
done

cat <<EOF | sudo tee -a /etc/default/isc-dhcp-server
INTERFACESv4="$GWIFS"
INTERFACESv6=""
INTERFACES="$GWIFS"
EOF

if [ $? -ne 0 ]
then
    echo "Failed to edit dhcp defaults file!"
    exit 1
fi

if [ -e /etc/init -a ! -e /etc/init/isc-dhcp-server6.override ]
then
    sudo bash -c 'echo "manual" > /etc/init/isc-dhcp-server6.override'
fi

sudo systemctl isc-dhcp-server start || \
    { echo "Failed to start ISC dhcpd!" && exit 1; }

sudo apt-get -y install --no-install-recommends iperf3

exit $?
