"""This profile instantiates an O-RAN multi-RU case with a DU/CU server, 5G server, traffic sink, jumphost/dhcp server, as connected via the PhantomNet attenuator matrix.  It includes a proxy node that serves DHCP, does NAT, and acts as a jumphost, that connects to the internal (experimental net) open fronthaul LAN(s) that the devices communicate on."""

import geni.portal as portal
import geni.urn as urn
import geni.rspec.igext as ig
import geni.rspec.pg as pg
import geni.rspec.emulab as elab
import geni.rspec.emulab.pnext as pn

MATRIX_RUS = ["atru1", "atru2"]
DENSE_RUS = []
RU_DEVICES = MATRIX_RUS + DENSE_RUS
CUDU_DEVICE = "flex02-meb"
CUDU_IFACES = "eth6 eth7"
UE_NODES = ["nuc22",]
CONSOLE_NODE = "nuc13"
DEFAULT_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"
PROXY_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"
COTS_UE_IMG = "urn:publicid:IDN+emulab.net+image+PowderTeam:cots-jammy-image"
PROXY_SCRIPT = "/local/repository/bin/proxy_start.sh"
CM = "urn:publicid:IDN+emulab.net"
CMNODEPREFIX = CM + "+node+"
CMIFACEPREFIX = CM + "+iface+"

PROXYGWPREFIX = "192.168"
PROXYGWSTART = 10
SHVLANNAME = ""
SHVLANMASK = "255.255.255.0"

# Parameters
pc = portal.Context()

# 5G Core host type
pc.defineParameter("core_hwtype", "5G Core Node Type",
                   portal.ParameterType.STRING, "d430",
                   longDescription="Hardware type for compute node that will host the 5G core.  Leave blank for any.")

# RU/FH Proxy/Jumphost host type
pc.defineParameter("proxy_hwtype", "Proxy Node Type",
                   portal.ParameterType.STRING, "d710",
                   longDescription="Hardware type for compute node that will act as the proxy. Leave blank for 'any'.")

# Console server for manually-connected RU serial interfaces
pc.defineParameter("console_node", "RU Console Host",
                   portal.ParameterType.STRING, CONSOLE_NODE,
                   longDescription="Serial console host for RU.")

# CU/DU device to allocate
portal.context.defineParameter(
    "cudu_device", "CU/DU Device", portal.ParameterType.STRING, CUDU_DEVICE,
    longDescription="CU/DU host to connect to RUs.")
portal.context.defineParameter(
    "cudu_fh_ifaces", "CU/DU Device FH Interfaces", portal.ParameterType.STRING, CUDU_IFACES,
    longDescription="Specific interfaces on the CU/DU node to connect to each RU.")

# Set of RU devices to allocate
portal.context.defineStructParameter(
    "ru_devices", "RU Devices", [{"id":x} for x in MATRIX_RUS],
    multiValue=True,
    min=0,
    multiValueTitle="RU Devices.",
    members=[
        portal.Parameter(
            "id",
            "RU Device",
            portal.ParameterType.STRING,
            RU_DEVICES[0], RU_DEVICES,
            longDescription="RU radio devices to allocate/connect via RF matrix."
        ),
    ])

# Set of UE devices to allocate
portal.context.defineStructParameter(
    "ue_nodes", "UE Nodes", [{"id":x} for x in UE_NODES],
    multiValue=True,
    min=0,
    multiValueTitle="UE Nodes.",
    members=[
        portal.Parameter(
            "id",
            "UE Node",
            portal.ParameterType.STRING,
            UE_NODES[0], UE_NODES,
            longDescription="NUC + COTS UE modem devices to allocate/connect via RF matrix."
        ),
    ])

pc.defineParameter("apn_name", "APN/DNN for COTS UE to use.",
                   portal.ParameterType.STRING, "internet",
                   longDescription="The Quectel connection manager will use this APN/DNN to obtain a PDU session.",
                   advanced=True)
pc.defineParameter("sharedVlanName","Shared VLAN Name",
                   portal.ParameterType.STRING,SHVLANNAME,
                   longDescription="This is the name of the shared VLAN that the separate RU experiments will attach to.",
                   advanced=True)
pc.defineParameter("sharedVlanNetmask", SHVLANMASK,
                   portal.ParameterType.STRING,"255.255.255.0",
                   advanced=True)
pc.defineParameter("createSharedVlan", "Create Shared VLAN",
                   portal.ParameterType.BOOLEAN, False,
                   longDescription="Create the shared VLAN. (Disable this if the VLAN already exists.)",
                   advanced=True)

params = pc.bindParameters()
pc.verifyParameters()
request = pc.makeRequestRSpec()

cudu_fh_ifaces = params.cudu_fh_ifaces.split(" ")

# Create the fronthaul LAN(s)
fhlans = []
for i in range(0, len(params.ru_devices)):
    fhlan = request.LAN("fhlan-%d" % (i,))
    fhlan.vlan_tagging = False
    fhlan.setNoBandwidthShaping()
    if params.sharedVlanName:
        shvlan_name = params.sharedVlanName + "-%d" % (i,)
        if params.createSharedVlan:
            fhlan.createSharedVlan(shvlan_name)
        else:
            fhlan.connectSharedVlan(shvlan_name)
    fhlans.append((fhlan, PROXYGWPREFIX + ".%d.1" % (PROXYGWSTART,), PROXYGWSTART))
    PROXYGWSTART += 1

# Request the NAT proxy node.
proxy = request.RawPC("proxy")
proxy.hardware_type = params.proxy_hwtype
proxy.disk_image = PROXY_IMG
proxy.addService(pg.Execute(shell="sh", command=PROXY_SCRIPT))
for i in range(0, len(fhlans)):
    (fhlan, gwaddr, octet) = fhlans[i]
    iface = proxy.addInterface("proxy-fh-if-%d" % (i,), pg.IPv4Address(gwaddr, params.sharedVlanNetmask))
    fhlan.addInterface(iface)

# Request the 5G Core device.
core = request.RawPC("core")
core.hardware_type = params.core_hwtype
corelan = request.LAN("corelan")
corelan.setNoBandwidthShaping()
corelan.bandwith = 10000000
iface = core.addInterface("corelan-core-if", pg.IPv4Address("192.168.100.1", "255.255.255.0"))
corelan.addInterface(iface)

# Request the CU/DU device.
cudu = request.RawPC("cudu")
cudu.component_id = CMNODEPREFIX + params.cudu_device
for i in range(0, len(fhlans)):
    (fhlan, gwaddr, octet) = fhlans[i]
    addr = PROXYGWPREFIX + ".%d.2" % (octet,)
    iface =  cudu.addInterface("cudu-fh-if-%d" % (i,), pg.IPv4Address(addr, params.sharedVlanNetmask))
    if i < len(cudu_fh_ifaces):
        #iface.component_id = CMIFACEPREFIX + params.cudu_device + ":" + cudu_fh_ifaces[i]
        iface.component_id = cudu_fh_ifaces[i]
    fhlan.addInterface(iface)
iface = cudu.addInterface("corelan-cudu-if", pg.IPv4Address("192.168.100.2", "255.255.255.0"))
corelan.addInterface(iface)

# Request the console node.
consolenode = request.RawPC("consoles")
consolenode.component_id = CMNODEPREFIX + params.console_node
consolenode.disk_image = DEFAULT_IMG

# Request the RU devices
matrix_ru_nodes = []
for i in range(0, len(params.ru_devices)):
    device = params.ru_devices[i]
    (fhlan, gwaddr, octet) = fhlans[i]
    ru = request.RawPC(device.id)
    ru.component_id = CMNODEPREFIX + device.id
    if device.id in MATRIX_RUS:
        matrix_ru_nodes.append(ru)
        ru.Desire("rf-controlled", 1)
    addr = PROXYGWPREFIX + ".%d.3" % (octet,)
    iface = ru.addInterface("ru-fh-if-%d" % (i,), pg.IPv4Address(addr, params.sharedVlanNetmask))
    fhlan.addInterface(iface)

# Request the nodes with the 5G module
ue_nodes = []
for idx,node in enumerate(params.ue_nodes):
    ue = request.RawPC("ue{}".format(idx+1))
    ue_nodes.append(ue)
    ue.component_id = CMNODEPREFIX + node.id
    ue.disk_image = COTS_UE_IMG
    ue.Desire("rf-controlled", 1)
    ue.addService(pg.Execute(shell="sh", command="sudo cp /local/repository/etc/default.script /etc/udhcpc/"))
    ue.addService(pg.Execute(shell="bash", command="/local/repository/bin/add-quectelcm-service.sh {}".format(params.apn_name)))

# Set attenuation to max on all RUs if more then one.
if len(matrix_ru_nodes) > 1:
    ue0 = ue_nodes[0]
    for ru in matrix_ru_nodes:
        ue0.addService(pg.Execute(shell="bash", command="/local/repository/bin/update-attens {} 95".format(ru.name)))
        pass
    pass

# Create the RF links between the COTS 5G modules and RU devices
idx = 1
for ru in matrix_ru_nodes:
    for ue in ue_nodes:
        rflink = request.RFLink("rflink{}".format(idx))
        rflink.addInterface(ru.addInterface("{}rf{}".format(ru.name,idx)))
        rflink.addInterface(ue.addInterface("{}rf{}".format(ue.name,idx)))
        idx += 1

# Print the RSpec to the enclosing page.
pc.printRequestRSpec()
